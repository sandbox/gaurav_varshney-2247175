<?php

/**
 * @file
 * Contains \Drupal\readmore\Plugin\field\formatter\ReadmoreFormatter.
 */

namespace Drupal\readmore\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'readmore' formatter.
 *
 * @FieldFormatter(
 *   id = "readmore",
 *   label = @Translation("Readmore"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary"
 *   },
 *   settings = {
 *     "trim_length" = "500",
 *     "readmore_trim_on_break" = FALSE,
 *     "readmore_show_readless" = FALSE,
 *     "readmore_ellipsis" = FALSE,
 *     "readmore_wordsafe" = FALSE
 *   }
 * )
 */
class ReadmoreFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, array &$form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['trim_length'] = array(
      '#type' => 'number',
      '#title' => t('Trim link text length'),
      '#field_suffix' => t('characters'),
      '#default_value' => $this->getSetting('trim_length'),
      '#min' => 1,
      '#description' => t('Leave blank to allow unlimited link text lengths.'),
    );
    $elements['readmore_trim_on_break'] = array(
      '#type' => 'checkbox',
      '#title' => t('Trim on @break', array('@break' => '<!--break-->')),
      '#description' => t('If @break not found in the text then trim length used.', array('@break' => '<!--break-->')),
      '#default_value' => $this->getSetting('readmore_trim_on_break'),
    );
    $elements['readmore_show_readless'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show read less'),
      '#default_value' => $this->getSetting('readmore_show_readless'),
    );
    $elements['readmore_ellipsis'] = array(
      '#type' => 'checkbox',
      '#title' => t('Add ellipsis'),
      '#default_value' => $this->getSetting('readmore_ellipsis'),
    );
    $elements['readmore_wordsafe'] = array(
      '#type' => 'checkbox',
      '#title' => t('Truncate on a word boundary'),
      '#default_value' => $this->getSetting('readmore_wordsafe'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    $settings = $this->getSettings();

    if (!empty($settings['trim_length'])) {
      $summary[] = t('Text trimmed to @limit characters', array('@limit' => $settings['trim_length']));
      $summary[] = $settings['readmore_trim_on_break'] ? t('Trim on @break', array('@break' => '<!--break-->')) : t('Do not trim on @break', array('@break' => '<!--break-->'));
      $summary[] = $settings['readmore_show_readless'] ? t('Show read less') : t('Do not show read less');
      $summary[] = $settings['readmore_ellipsis'] ? t('Add ellipsis') : t('Do not add ellipsis');
      $summary[] = $settings['readmore_wordsafe'] ? t('Safe words') : t('Do not safe words');
    }
    else {
      $summary[] = t('Text not trimmed');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = array();
    $settings = $this->getSettings();

    $max_length = $settings['trim_length'];

    foreach ($items as $delta => $item) {
      $text = $item->processed;
      $text_length = drupal_strlen($item->processed);
      $trim_length = $settings['trim_length'];

      // Don't do anything if text length less than defined.
      if ($text_length > $trim_length) {
        // Add Read less if need.
        if ($settings['readmore_show_readless']) {
          $text .= ' ' . l(t('Read less'), current_path(), array('attributes' => array('class' => array('readless-link'))));
        }

        // Get trimmed string.
        $summary = $this->readmore_truncate_string(
            $text, isset($item->format) ? $item->format : NULL, $trim_length, $settings['readmore_wordsafe'], $settings['readmore_trim_on_break']
        );

        // Add readmore link.
        $summary .= '<span>';
        $summary .= $settings['readmore_ellipsis'] ? t('...') : NULL;
        $summary .= l(t('read more'), current_path(), array('attributes' => array('class' => array('readmore-link'))));
        $summary .= '</span>';

        // Close all HTML tags.
        $summary = Html::normalize($summary);

        $elements[$delta] = array(
          '#theme' => 'readmore',
          '#text' => $text,
          '#summary' => $summary,
          '#attached' => array(
            'js' => array(
              drupal_get_path('module', 'readmore') . '/js/readmore.js' => array(),
            ),
          ),
        );
        
      }
      else {
        $elements[$delta] = array(
          '#markup' => $text,
        );
      }
    }

    return $elements;
  }

  /**
   * Truncate string by a number of characters.
   *
   * @param string $text
   *   The string to truncate.
   * @param string $format
   *   The format of the content.
   * @param int $size
   *   An upper limit on the returned string length.
   * @param bool $wordsafe
   *   If TRUE, attempt to truncate on a word boundary.
   *
   * @return string
   *   Return truncated string.
   */
  protected function readmore_truncate_string($text, $format = NULL, $size = NULL, $wordsafe = FALSE, $use_break = TRUE) {
    if (!isset($size)) {
      // If size is not set then use default.
      $size = 500;
    }

    if ($use_break) {
      // Find where the delimiter is in the body.
      $delimiter = strpos($text, '<!--break-->');

      if ($delimiter) {
        // Set new size.
        $size = $delimiter;
      }
    }

    // We check for the presence of the PHP evaluator filter in the current
    // format. If the body contains PHP code just return as is.
    if (isset($format)) {
      $filters = entity_load('filter_format', $format);
      if (isset($filters) && $filters->status && strpos($text, '<?') !== FALSE) {
       return $text;
      }
    }

    // The summary may not be longer than maximum length specified. Initial slice.
    $summary = truncate_utf8($text, $size, $wordsafe, FALSE);

    if ($wordsafe) {
      // Store the actual length of the truncated string.
      $max_rpos = drupal_strlen($summary);

      // How much to cut off the end of the summary so that it doesn't end in the
      // middle of a paragraph, sentence, or word.
      // Initialize it to maximum in order to find the minimum.
      $min_rpos = $max_rpos;

      // Store the reverse of the summary.
      $reversed = strrev($summary);

      // Build an array of arrays of break points grouped by preference.
      $break_points = array();

      // A paragraph near the end of sliced summary is most preferable.
      $break_points[] = array('</p>' => 0);

      // If no complete paragraph then treat line breaks as paragraphs.
      $line_breaks = array('<br />' => 6, '<br>' => 4);
      // Newline only indicates a line break if line break converter
      // filter is present.
      if (\Drupal::entityQuery('filter_format')->condition('format', 'filter_autop')->execute()) {
        $line_breaks["\n"] = 1;
      }
      $break_points[] = $line_breaks;

      // If the first paragraph is too long, split at the end of a sentence.
      $break_points[] = array(
        '. ' => 1,
        '! ' => 1,
        '? ' => 1,
        '。' => 0,
        '؟ ' => 1,
      );

      // Iterate over the groups of break points until a break point is found.
      foreach ($break_points as $points) {
        // Look for each break point, starting at the end of the summary.
        foreach ($points as $point => $offset) {
          // The summary is already reversed, but the break point isn't.
          $rpos = strpos($reversed, strrev($point));
          if ($rpos !== FALSE) {
            $min_rpos = min($rpos + $offset, $min_rpos);
          }
        }

        // If a break point was found in this group, slice and stop searching.
        if ($min_rpos !== $max_rpos) {
          // Don't slice with length 0. Length must be <0 to slice from RHS.
          $summary = ($min_rpos === 0) ? $summary : drupal_substr($summary, 0, 0 - $min_rpos);
          break;
        }
      }
    }
    return $summary;
  }

}
